# Lines configured by zsh-newuser-install
HISTFILE=~/.zsh_history
HISTSIZE=1000
SAVEHIST=900000000000000
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
# End of lines added by compinstall
#zstyle :compinstall filename '/home/$(whoami)/.zshrc'
zstyle :compinstall filename '/Users/$(whoami)/.zshrc'

autoload -Uz compinit
compinit


# changeing default text editor to vim
export EDITOR=vim

#suggested python thingy
PYTHONPATH=$HOME/lib/python
EDITOR=vim

export PYTHONPATH EDITOR

# zsh options
setopt notify
setopt correct
setopt auto_cd
setopt auto_list

#this is to make zsh preform more like bash in that it does not show % at the end of unfinished printed lines
unsetopt prompt_cr prompt_sp


# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.

# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char


#aliases

#alias for ls pretty
alias ls="exa"


#zsh syntax hightlighting and auto suggest
source ~/.zshScripts/zsh-syntax-highlighting/zsh-syntax-highlighting.plugin.zsh
source ~/.zshScripts/zsh-autosuggestions/zsh-autosuggestions.zsh

#ps1 prompt
PS1="%B%F{red}%n@%m%f%b:%1~%'$ "


export PATH="/Users/$(whoami)/.local/bin:/usr/local/opt/python@3.10/bin:/Users/$(whoami)/.local/bin:$PATH"
